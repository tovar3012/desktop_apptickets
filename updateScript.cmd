@echo off
del Manual.pdf
del Newtonsoft.Json.dll
del Newtonsoft.Json.xml
del System.Net.Http.Formatting.dll
del System.Net.Http.Formatting.xml
del TicketsClient.exe
del TicketsClient.exe.config
del TicketsClient.pdb
git clone https://gitlab.com/tovar3012/desktop_apptickets.git
move desktop_apptickets\* .
rmdir /s /q desktop_apptickets
exit